﻿var gulp = require('gulp');
var tsc = require('gulp-typescript');
var tsproject = tsc.createProject('tsconfig.json');
var config = require('./gulpe.config')();



gulp.task('compile-ts', function () {
    var sourceTsfiles = [config.allTs, config.typings];
    var tsResult = gulp.Src(sourceTsfiles).pipe(tsc(tsproject));
    return tsResult.js
        .pipe(gulp.dest(config.tsOutputpath));
})