﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Angular3rdTest.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult ContactUs()
        {
            return View();
        }
        public ActionResult HeroesComponent()
        {
            return View();
        }
        public ActionResult HeroDetailComponent(int id)
        {
            return View();
        }
        public ActionResult DashboardComponent()
        {
            return View();
        }
        public JsonResult Heroes()
        {
            List<Hero> herolist = new List<Hero>()
            {
                new Hero()
                {
                    id =1,
                    name="ashutosh"
                },
                new Hero()
                {
                    id=2,
                    name="Ganesh"
                },
                 new Hero()
                {
                    id=3,
                    name="Ganesh1"
                }
            };
            return Json(new { data = herolist }, JsonRequestBehavior.AllowGet);
            
        }
        public class Hero
        {
            public int id { get; set; }
            public string name { get; set; }
        }

    }
}